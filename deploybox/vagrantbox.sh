#!/usr/bin/env bash

set -e

sudo apt-get update
sudo apt-get -y install python3-setuptools python3-pip libpq-dev
sudo pip3 install -r /vagrant/requirements.txt
sudo apt-get -y install postgresql
sudo cp /vagrant/deploybox/postgresql/pg_hba.conf /etc/postgresql/9.5/main/
sudo cp /vagrant/deploybox/postgresql/postgresql.conf /etc/postgresql/9.5/main/
sudo service postgresql restart

DATABASE_NAME=`cat /vagrant/device_server/config.py | grep 'DATABASE_NAME =' | cut -d '"' -f2`
DB_USER_NAME=`cat /vagrant/device_server/config.py | grep 'DB_USER_NAME =' | cut -d '"' -f2`
DB_USER_PASS=`cat /vagrant/device_server/config.py | grep 'DB_USER_PASS =' | cut -d '"' -f2`

CONNECTIONS_TABLE_NAME=`cat /vagrant/device_server/config.py | grep 'CONNECTIONS_TABLE_NAME =' | cut -d '"' -f2`
DATA_TABLE_NAME=`cat /vagrant/device_server/config.py | grep 'DATA_TABLE_NAME =' | cut -d '"' -f2`


cat << EOF | sudo psql -U postgres
CREATE USER $DB_USER_NAME
WITH PASSWORD '$DB_USER_PASS';

CREATE DATABASE $DATABASE_NAME WITH OWNER=$DB_USER_NAME
                               LC_COLLATE='en_US.utf8'
                               LC_CTYPE='en_US.utf8'
                               ENCODING='UTF8'
                               TEMPLATE=template0;

\c $DATABASE_NAME $DB_USER_NAME
                           
CREATE TABLE public.$CONNECTIONS_TABLE_NAME
(
    id serial NOT NULL,
    connection_established timestamp with time zone NOT NULL DEFAULT now(),
    connection_aborted timestamp with time zone,
    address cidr NOT NULL,
    port integer NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE public.$DATA_TABLE_NAME
(
    id serial NOT NULL,
    connection bigint NOT NULL,
    received timestamp with time zone NOT NULL DEFAULT now(),
    data json NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (connection)
        REFERENCES public.$CONNECTIONS_TABLE_NAME (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

EOF

(cd /vagrant && sudo python3 setup.py install)