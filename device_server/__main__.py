from device_server import server, config
import argparse
import logging
from os.path import abspath, dirname


def main():
    parser = argparse.ArgumentParser(description="Start server device, "
                                                 "config file: {}/config.py".format(dirname(abspath(__file__))))

    parser.add_argument("--logs", choices=["INFO", "ERROR"], default="INFO", help="Logging level")

    args = parser.parse_args()

    logging.basicConfig(level=args.logs)

    server.main(config)


if __name__ == '__main__':
    main()
