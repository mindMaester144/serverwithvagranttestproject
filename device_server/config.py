ADDRESS = "localhost"
PORT = 9000

CONNECTIONS_TABLE_NAME = "connections"
DATA_TABLE_NAME = "data"

DATABASE_NAME = "device_db"
DB_USER_NAME = "device_db_user"
DB_USER_PASS = "qwerty"
DB_ADDRESS = "localhost"
DB_PORT = "5432"

DB_CONNECTION_CONFIG = "dbname='{}' host='{}' port='{}' user='{}' password='{}'".format(DATABASE_NAME,
                                                                                        DB_ADDRESS,
                                                                                        DB_PORT,
                                                                                        DB_USER_NAME,
                                                                                        DB_USER_PASS)
