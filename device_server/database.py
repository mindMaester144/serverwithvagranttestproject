import psycopg2
import json


class DatabaseHandler(object):
    Error = psycopg2.Error

    def __init__(self, database_config):
        self.database_connection_config = database_config.DB_CONNECTION_CONFIG
        self.connection_table_name = database_config.CONNECTIONS_TABLE_NAME
        self.data_table_name = database_config.DATA_TABLE_NAME
        self.connection = None

    def __enter__(self):
        self.connection = psycopg2.connect(self.database_connection_config)
        self.connection.autocommit = True
        return self

    def __exit__(self, type, value, traceback):
        self.connection.close()

    def cursor(self):
        return self.connection.cursor()

    def write_db_connection_aborted(self, cursor, db_element_id):
        cursor.execute("UPDATE {} SET connection_aborted = now() "
                       "WHERE id = {}".format(self.connection_table_name, db_element_id))

    def write_db_connection_established(self, cursor, ip, port):
        cursor.execute("INSERT INTO {} "
                       "(address, port) "
                       "VALUES ('{}', {}) "
                       "RETURNING id".format(self.connection_table_name, ip, port))

        return cursor.fetchall()[0][0]

    def write_db_data_from_message(self, cursor, db_element_id, data):
        cursor.execute("INSERT INTO {} "
                       "(connection, received, data) "
                       "VALUES ({}, now(), '{}')".format(self.data_table_name, db_element_id, json.dumps(data)))
