from device_server import config as default_config, database as database_default_module
import socket
import threading
import json
import logging
from datetime import datetime

ADDRESS_ALREADY_IN_USE_ERROR_NUM = 98
SOCKET_SHUTDOWN_ERROR_NUM = 22
BAD_FILE_DESCRIPTION_ERROR_NUM = 9


def main(config):
    stop_event = threading.Event()

    threading.Thread(target=start_server, args=(config, database_default_module.DatabaseHandler, stop_event)).start()

    logging.info("'Enter' to stop.")
    input()
    stop_event.set()


def start_server(config, db_handler_class, stop_event, server_stopped_event=threading.Event()):
    try:
        with socket.socket() as sock, db_handler_class(config) as db_handler:
            sock.bind((config.ADDRESS, config.PORT))
            sock.listen(1)

            connections_closed_confirm_event = threading.Event()

            threading.Thread(target=listen_to_new_connections,
                             args=(
                                 sock,
                                 db_handler,
                                 stop_event,
                             ),
                             kwargs={
                                 "connections_closed_confirm_event": connections_closed_confirm_event,
                             }).start()

            stop_event.wait()
            sock.shutdown(socket.SHUT_RDWR)
            connections_closed_confirm_event.wait()

    except OSError as e:
        if e.errno == ADDRESS_ALREADY_IN_USE_ERROR_NUM:
            logging.error("Address '{}:{}' already in use!".format(config.ADDRESS, config.PORT))
            exit(1)

        raise

    except db_handler_class.Error as e:
        logging.error("Connection error: {}".format(e))
        exit(1)

    finally:
        stop_event.set()

    server_stopped_event.set()
    logging.info("Socket closed.")


def listen_to_new_connections(sock, db_handler, stop_event, connections_closed_confirm_event=threading.Event()):
    active_connections = []
    last_active_connection_check = datetime.now()
    while not stop_event.is_set():
        try:
            connection, address = sock.accept()

            logging.info("Connected: {}!".format(address))

            stop_confirmed_event = threading.Event()
            active_connections.append(stop_confirmed_event)

            threading.Thread(target=get_messages,
                             args=(
                                 connection,
                                 address,
                                 db_handler,
                                 stop_event,
                             ),
                             kwargs={
                                 "stop_confirmed_event": stop_confirmed_event,
                             }).start()

            if datetime.now().minute - last_active_connection_check.minute > 1:
                active_connections = list(filter(lambda x: not x.isSet(), active_connections))
                last_active_connection_check = datetime.now()

        except ConnectionAbortedError:
            pass

        except OSError as e:
            if e.errno == SOCKET_SHUTDOWN_ERROR_NUM:
                pass

            else:
                raise

    logging.info("Stop listening, waiting for connections to close...")

    for active_connection in filter(lambda x: not x.isSet(), active_connections):
        active_connection.wait()

    connections_closed_confirm_event.set()
    logging.info("All connections are closed.")


def get_messages(connection, address, db_handler, stop_event, stop_confirmed_event=threading.Event()):
    with db_handler.cursor() as db_cursor:
        db_element_id = db_handler.write_db_connection_established(db_cursor, address[0], address[1])

        while not stop_event.is_set():
            try:
                data = connection.recv(1024)

                if not data:
                    break

                db_handler.write_db_data_from_message(db_cursor, db_element_id, data.decode())
                logging.info("{} - {}".format(address, data.decode()))

            except OSError as e:
                if e.errno == BAD_FILE_DESCRIPTION_ERROR_NUM:
                    break

                raise

        db_handler.write_db_connection_aborted(db_cursor, db_element_id)

    connection.close()
    stop_confirmed_event.set()
    logging.info("Disconnected: {}!".format(address))


if __name__ == "__main__":
    logging.basicConfig(level='INFO')
    main(default_config)
