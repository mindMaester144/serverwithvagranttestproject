import socket
import threading
from device_user import config as default_config
import logging

SOCKET_CLOSED_ERROR_NUM = 9


def main(config):
    stop_event = threading.Event()

    threading.Thread(target=start_user, args=(config, stop_event, "Data")).start()

    logging.info("Enter to stop.")
    input()
    stop_event.set()


def start_user(config, stop_event, data_to_send):
    with socket.socket() as sock:
        try:
            sock.connect((config.ADDRESS, config.PORT))
            logging.info("Connected to '{}:{}'!".format(config.ADDRESS, config.PORT))

            send_data(sock, config.N, stop_event, data_to_send)

        except ConnectionRefusedError:
            logging.error("Connection '{}:{}' refused!".format(config.ADDRESS, config.PORT))
            exit(1)

        except ConnectionResetError as e:
            logging.error("Connection reset by peer!")
            exit(1)

        except BrokenPipeError:
            logging.error("Connection lost!")
            exit(1)

        finally:
            stop_event.set()


def send_data(sock, time_to_waite, stop_event, data_to_send):
    while not stop_event.wait(time_to_waite):
        try:
            sock.send(data_to_send.encode())
            logging.info("Data sent.")

        except OSError as e:
            if e.errno == SOCKET_CLOSED_ERROR_NUM:
                logging.error("Socket closed!")
                break
            else:
                raise


if __name__ == "__main__":
    logging.basicConfig(level="INFO")
    main(default_config)
