from setuptools import setup, find_packages
import device_server
import device_user


setup(
    name="device_user",
    version=device_user.__version__,
    include_package_data=True,
    zip_safe=False,
    entry_points={
        "console_scripts":
            ["user-device = device_user.__main__:main",]
        }
    )

setup(
    name="device_server",
    version=device_server.__version__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    entry_points={
        "console_scripts":
            ["server-device = device_server.__main__:main",]
        }
    )

setup(
    name="test_module",
    version=device_server.__version__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    entry_points={
        "console_scripts":
            ["test-server = test_module.__main__:main",]
        }
    )
