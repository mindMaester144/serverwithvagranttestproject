import argparse
from test_module import test
import argparse
import logging
from os.path import abspath, dirname


def main():
    parser = argparse.ArgumentParser(description="Test server power, "
                                                 "config file: {}/config.py".format(dirname(abspath(__file__))))

    parser.add_argument("--logs",
                        choices=["INFO", "ERROR", "CRITICAL"],
                        default="CRITICAL",
                        help="Logging level")

    parser.add_argument("--N",
                        type=float,
                        default=None,
                        help="Users send data per N seconds")

    parser.add_argument("--users",
                        type=int,
                        default=None,
                        help="How many users connected to the server while test([0..500])")

    args = parser.parse_args()

    if (args.users is not None) and (not 0 <= args.users <= 500):
        print("Wrong amount of users(0 <= users <= 500)!")
        exit(1)

    if (args.N is not None) and (0 > args.N):
        print("N can't be negative!")
        exit(1)

    logging.basicConfig(level=args.logs)

    test.start_test(args.N, args.users)


if __name__ == "__main__":
    main()
