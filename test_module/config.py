import random
from device_server import config

# general
ADDRESS = "localhost"
PORT = 8000 + random.randint(1, 1000)
TEST_DATA = "test"

# user_activity
ACTIVITY_N = 0.05
ACTIVITY_COUNT = 100

# test_user
TEST_USER_N = 1

# server
CONNECTIONS_TABLE_NAME = "test_connections"
DATA_TABLE_NAME = "test_data"

DATABASE_NAME = config.DATABASE_NAME
DB_USER_NAME = config.DB_USER_NAME
DB_USER_PASS = config.DB_USER_PASS
DB_ADDRESS = config.DB_ADDRESS
DB_PORT = config.DB_PORT

DB_CONNECTION_CONFIG = "dbname='{}' host='{}' port='{}' user='{}' password='{}'".format(DATABASE_NAME,
                                                                                        DB_ADDRESS,
                                                                                        DB_PORT,
                                                                                        DB_USER_NAME,
                                                                                        DB_USER_PASS)



