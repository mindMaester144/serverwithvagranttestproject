from device_server import server, database
from device_user import user
from test_module import config
from datetime import datetime
import threading
import time
import socket
import json
import unittest
import psycopg2


def start_test(n, count):
    TestServerPower.USERS_COUNT = count
    TestServerPower.USERS_N = n

    suite = unittest.TestSuite()
    suite.addTest(TestServerPower("test_server_power"))

    unittest.TextTestRunner().run(suite)


class TestServerPower(unittest.TestCase):
    USERS_COUNT = None
    USERS_N = None

    def setUp(self):
        with psycopg2.connect(DatabaseConfig.DB_CONNECTION_CONFIG) as connection:
            connection.autocommit = True

            cursor = connection.cursor()
            self.drop_test_database_tables(cursor)
            self.create_test_database_tables(cursor)

    def tearDown(self):
        with psycopg2.connect(DatabaseConfig.DB_CONNECTION_CONFIG) as connection:
            connection.autocommit = True

            cursor = connection.cursor()
            self.drop_test_database_tables(cursor)

    def test_server_power(self, n=None, user_count=None):
        """
            This function starts server and a group of users to emulate real server with real connections.
            Then it creates test user and connects it to the server. After connection
            test user sends test data, to find out ping.
        """
        print("------Test started-------")
        server_config = ServerConfig()
        user_activity_config = UserActivityConfig()
        test_user_config = TestUserConfig()

        if self.USERS_N is not None:
            user_activity_config.N = self.USERS_N
        if self.USERS_COUNT is not None:
            user_activity_config.USERS_COUNT = self.USERS_COUNT

        test_server = TestServer(server_config)
        user_activity = UsersActivity(user_activity_config)
        test_user = TestUser(test_user_config, server_config)

        test_server.start()
        user_activity.start()

        test_result = test_user.test()

        user_activity.stop()
        test_server.stop_and_wait()

        print("Total users {}, N:{}".format(user_activity_config.USERS_COUNT, user_activity_config.N))

        if test_result["refused"]:
            print("Connection refused.")

        else:
            print("Connecting time:{}mcs".format(test_result["connection_ping"]))

            if test_result["timeout"]:
                print("Data didn't come(timeout {} sec)".format(test_user.wait_data_timeout))

            else:
                print("Sending data time:{}mcs".format(test_result["data_ping"]))

    @staticmethod
    def create_test_database_tables(cursor):
        cursor.execute("CREATE TABLE public.{}"
                       "("
                       "    id serial NOT NULL,"
                       "    connection_established timestamp with time zone NOT NULL DEFAULT now(),"
                       "    connection_aborted timestamp with time zone,"
                       "    address cidr NOT NULL,"
                       "    port integer NOT NULL,"
                       "    PRIMARY KEY (id)"
                       ");"
                       "CREATE TABLE public.{}"
                       "("
                       "    id serial NOT NULL,"
                       "    connection bigint NOT NULL,"
                       "    received timestamp with time zone NOT NULL DEFAULT now(),"
                       "    data json NOT NULL,"
                       "    PRIMARY KEY (id),"
                       "    FOREIGN KEY (connection)"
                       "        REFERENCES public.{} (id) MATCH SIMPLE"
                       "        ON UPDATE NO ACTION"
                       "        ON DELETE NO ACTION"
                       ");".format(DatabaseConfig.CONNECTIONS_TABLE_NAME,
                                   DatabaseConfig.DATA_TABLE_NAME,
                                   DatabaseConfig.CONNECTIONS_TABLE_NAME))

    @staticmethod
    def drop_test_database_tables(cursor):
        try:
            cursor.execute("DROP TABLE {} CASCADE;".format(DatabaseConfig.CONNECTIONS_TABLE_NAME))
        except psycopg2.ProgrammingError as e:
            pass

        try:
            cursor.execute("DROP TABLE {} CASCADE;".format(DatabaseConfig.DATA_TABLE_NAME))
        except psycopg2.ProgrammingError as e:
            pass


class TestServer(object):
    def __init__(self, server_config, stop_server_event=threading.Event()):
        self.config = server_config
        self.stop_server_event = stop_server_event
        self.server_stopped_event = threading.Event()

    def start(self):
        threading.Thread(target=server.start_server,
                         args=(self.config, TestDatabaseHandler, self.stop_server_event),
                         kwargs={"server_stopped_event": self.server_stopped_event}).start()

        time.sleep(1)

    def stop(self):
        self.stop_server_event.set()

    def stop_and_wait(self):
        self.stop()
        self.server_stopped_event.wait()


class UsersActivity(object):
    def __init__(self, users_activity_config):
        self.config = users_activity_config
        self.users_count = users_activity_config.USERS_COUNT
        self.stop_user_events = []

    def start(self):
        self.stop_user_events = []

        for x in range(self.users_count):
            stop_user_event = threading.Event()
            self.stop_user_events.append(stop_user_event)
            threading.Thread(target=user.start_user, args=(self.config, stop_user_event, "data")).start()

        time.sleep(1)

    def stop(self):
        for event in self.stop_user_events:
            event.set()


class TestUser(object):
    def __init__(self, test_user_config, server_config, stop_user_event=threading.Event()):
        self.config = test_user_config
        self.data_to_send = test_user_config.TEST_DATA
        self.wait_data_timeout = test_user_config.WAIT_DATA_TIMEOUT
        self.stop_user_event = stop_user_event
        self.server_config_object = server_config

    def test(self):
        try:
            with socket.socket() as sock:
                connection_start_time = datetime.now()
                sock.connect((self.config.ADDRESS, self.config.PORT))
                connection_end_time = datetime.now()

                sending_start_time = datetime.now()
                threading.Thread(target=user.send_data,
                                 args=(sock, self.config.N, self.stop_user_event, self.data_to_send)).start()

                test_data_came = self.server_config_object.test_data_received_event.wait(self.wait_data_timeout)

                self.stop_user_event.set()

                connection_time = (connection_end_time - connection_start_time).microseconds

            if test_data_came:
                data_ping = (self.server_config_object.test_data_received_time - sending_start_time).microseconds

            else:
                data_ping = None

            is_refused = False

        except ConnectionRefusedError as e:
            is_refused = True
            data_ping = None
            connection_time = None
            test_data_came = False

        return {
            "connection_ping": connection_time,
            "data_ping": data_ping,
            "timeout": not test_data_came,
            "refused": is_refused,
        }


class DatabaseConfig(object):
    CONNECTIONS_TABLE_NAME = config.CONNECTIONS_TABLE_NAME
    DATA_TABLE_NAME = config.DATA_TABLE_NAME
    DATABASE_NAME = config.DATABASE_NAME
    DB_USER_NAME = config.DB_USER_NAME
    DB_USER_PASS = config.DB_USER_PASS
    DB_ADDRESS = config.DB_ADDRESS
    DB_PORT = config.DB_PORT
    DB_CONNECTION_CONFIG = config.DB_CONNECTION_CONFIG


class SocketConfig(object):
    ADDRESS = config.ADDRESS
    PORT = config.PORT


class UserActivityConfig(SocketConfig):
    N = config.ACTIVITY_N
    USERS_COUNT = config.ACTIVITY_COUNT


class TestUserConfig(SocketConfig):
    N = config.TEST_USER_N
    WAIT_DATA_TIMEOUT = 20
    TEST_DATA = config.TEST_DATA


class ServerConfig(SocketConfig, DatabaseConfig):
    TEST_DATA = config.TEST_DATA

    def __init__(self):
        self.test_data_received_event = threading.Event()
        self.test_data_received_time = None


class TestDatabaseHandler(database.DatabaseHandler):
    def __init__(self, database_config):
        super().__init__(database_config)
        self.test_data = database_config.TEST_DATA
        self.config_object = database_config

    def write_db_data_from_message(self, cursor, db_element_id, data):
        cursor.execute("INSERT INTO {} "
                       "(connection, received, data) "
                       "VALUES ({}, now(), '{}')".format(self.data_table_name, db_element_id, json.dumps(data)))

        if data == self.test_data and not self.config_object.test_data_received_event.isSet():
            self.config_object.test_data_received_event.set()
            self.config_object.test_data_received_time = datetime.now()



